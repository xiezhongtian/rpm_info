# 软件包管理

- **源码包管理**：软件包按 sig 进行分类，新增仓库、修改仓库所属sig/衰退仓库、删除仓库都需要修改 rpm_info/packages.yaml 文件实现
- **二进制包管理**：二进制包由 `baseos.list`、`appstream.list`、`epol.list`、`deprecated.list` 四个文件管理所属源

## 加入仓库

我们欢迎并鼓励开发者参与我们的仓库开发。我们使用一个名为`collaborators.yaml`的文件来记录所有参与仓库开发的外部成员，该文件使用 YAML 格式记录。


要申请成为仓库开发者，请按照以下步骤操作：

1. fork 仓库：`https://gitee.com/OpenCloudOS/rpm_info`
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. 编辑 `collaborators.yaml` 文件，添加您的信息。请确保正确填写您的 gitee 用户名称、邮箱和希望加入的仓库名称。
4. 提交 pull request 并**提供修改原因**


第一次申请加入仓库时，需要填写个人信息，请以该模板为参考，在 `collaborators.yaml` 文件末尾追加以下内容：
```yaml
- 用户名称: xxx
  联系方式: xxx
  仓库:
    - xxx
    - xxx
```

示例：
```yaml
- 用户名称: gitee-bot
  联系方式: gitee-bot@example.com
  仓库:
    - setup
```

字段说明：

- `用户名称`：Gitee 账户名称，**请务必保证与 gitee 用户名称一致**，否则无法正确为您修改权限。以 [gitee-bot](https://gitee.com/gitee-bot) 用户为例，个人主页中，左上角个人介绍中 `@gitee-bot` 表示该用户名称为 `gitee-bot`，与个人空间地址一致，而 `Gitee GPG Bot` 则是账户昵称，非唯一标识。
- `联系方式`：邮箱或手机号码。用于与团队沟通。
- `仓库`：申请加入的仓库列表。


注意事项：

- 在修改`collaborators.yaml`文件时，**请不要修改其他成员的信息。**
- 如有任何疑问或需要帮助，请随时与我们联系。


## 新增软件包 
1. **自检查**：确认当前仓库及 packages.yaml 文件中**不存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：修改 packages.yaml，将新增软件包名添加到合适的 sig-packages 列表尾端
4. **二进制包管理**：新增软件包默认进入EPOL仓库，无需手动修改
4. 提交 pull request：记录**新增原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化新增软件包，返回仓库链接

## 衰退软件包
1. **自检查**：确认当前仓库及 packages.yaml 文件中**存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：修改 packages.yaml，将衰退的软件包移动到 deprecated sig
4. **二进制包管理**：将衰退软件包对应的所有二进制软件，从原始列表移到deprecated.list中，并确认没有其他二进制包依赖该衰退软件包。（比如，假设appstream-data这个软件需要衰退，它存在于appstream.list中，那么就需要删除appstream.list中 appstream-data 所有二进制包，添加到 deprecated.list，才算完成 appstream-data 的衰退操作。）
4. 提交 pull request：记录**衰退原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化衰退软件包


## 删除软件包
1. **自检查**：确认该软件包已经被**衰退**（仅允许删除 deprecated sig 中已经被衰退的软件包），并且有必要删除
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：修改 packages.yaml，将软件包名所在行删除
4. **二进制包管理**：将被删除软件包对应的所有二进制包，从所在列表中删除。
4. 提交 pull request：记录**删除原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化删除软件包

## 修改软件包

### 修改软件包 sig
1. **自检查**：确认当前仓库及 packages.yaml 文件中**存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：修改 packages.yaml，将软件包从原始sig中删除，并添加到新sig中
4. **二进制包管理**：仅修改软件包 sig，无需修改二进制包
4. 提交 pull request：记录**修改原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化修改软件包 sig


### 修改二进制包所在源
1. **自检查**：确认当前仓库及 packages.yaml 文件中**存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：无需修改 packages.yaml
4. **二进制包管理**：确认移动后不影响其他软件包的安装，从原始列表移到新列表中。（例如，BaseOS 中的软件包不能依赖其他源中的软件包，AppStream 中的软件包仅依赖 BaseOS + AppStream 源）
4. 提交 pull request：记录**修改原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化移动软件包所在源


## 常见问题
- 某些软件包无法解析
    - 如果无法解析软件包，请检查 **自检查** 条件是否满足，以及修改内容是否包含空格等无效字符，若仍然有问题请提issue方便后续跟踪处理
- 能否同时增加、删除、修改软件包
    - 可以。请确认流水线返回解析结果符合预期即可。此外建议一个 PR 只做一件事情